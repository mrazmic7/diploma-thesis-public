nuget packages used in this project:

Optimized Priority Queue by BlueRaja
is used for managing loading chunks in order of distance from player.
source: https://www.nuget.org/packages/OptimizedPriorityQueue/
license: MIT

Newtonsoft.Json by James Newton-King
is used for processing terrain elevation data.
source: https://www.nuget.org/packages/Newtonsoft.Json/
license: MIT

NGeoHash by Jesse Emerick, Leonardo Rosales, Robin Michael
is used for dividing the surface of Earth into a grid of chunks.
source: https://www.nuget.org/packages/NGeoHash
license: MIT 

MathNet.Numerics by Math.NET Project
is used for calculating samples from normal distribution.
source: https://www.nuget.org/packages/MathNet.Numerics/
license: MIT