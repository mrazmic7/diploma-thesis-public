/// <summary>
/// A struct that holds two double values.
/// </summary>
/// <author>Michal Mráz</author>
public struct Double2
    {
        public double X;
        public double Y;

        public Double2(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
