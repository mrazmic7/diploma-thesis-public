﻿/// <summary>
/// Settings, some can be set in a settings window, some can be set only here in code.
/// </summary>
/// <author>Michal Mráz</author>
static public class Settings
{
    public static string googleApiKey = "ENTER YOUR GOOGLE ELEVATION API KEY HERE";
    public static float chunkGenerationDistance;
    public static float vegetationDensity;
    public static float vegetationLushness;
    public static bool useBuildingHeights;
}
