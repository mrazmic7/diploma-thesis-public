using UnityEngine;

/// <summary>
/// street vertices
/// </summary>
/// <author>Michal Mráz</author>
public class StreetData : MonoBehaviour
{
    public Vector3[] vertices;
}
