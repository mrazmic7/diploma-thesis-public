About the program:
The goal of this project is to design and implement a system that procedurally generates game environment.
The environment is geolocalized with biomes and vegetation.
The target devices are mobile phones where the generation is done in real time. 
The generated environment is then tested with real users using a simple game prototype.

Some folders have their own README, ATTRIBUTION or LICENSE files. Please read them for more information.

The code was written with the help of AI tools ChatGPT and GitHub Copilot.

The user manual can be found in appendix B of the thesis. 

The program downloads data from Google Elevation API and for that it needs an API key. 
You can enter your Google API key in the file Settings.cs that can be found here: Assets/Scripts/Common